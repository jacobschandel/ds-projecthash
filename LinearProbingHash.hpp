/**
 * @file LinearProbingHash.hpp
 * @author Jacob H. Schandel (jhs51@uakron.edu)
 * @brief Linear Probing Hash for Project 3: Hash
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <vector>

/**
 * @brief Basic hash table with linear probing
 */
class LinearProbingHash
{
public:

    /**
     * @brief Construct a new Linear Probing Hash Table object
     */
    LinearProbingHash();

    /**
     * @brief Construct a new Linear Probing Hash Table object
     * 
     * @param size number of slots in table
     */
    LinearProbingHash(int size);

    /**
     * @brief Inserts an element into the hash table
     * 
     * @param value value to insert
     */
    void insert(int value);

    /**
     * @brief Removes an element from the hash table
     * 
     * @param value value to remove
     * @return int of removed value
     */
    int remove(int value);

private:
    
    /**
     * @brief Array containing hash table
     */
    int * hashTable;

    /**
     * @brief Size of hash table
     */
    int size;

    /**
     * @brief If true, corresponding element in hashTable is occupied
     */
    bool * inUse;

    /**
     * @brief Find a hash for a given value
     * 
     * @param value number to find
     * @return int containing hash value
     */
    int hash(int value);

    /**
     * @brief Function to probe the hash table for a given value and its hash
     * 
     * @param value number to find
     * @param hashValue hash value
     * @return int containing final probed value
     */
    int probe(int value, int hashValue);
};
