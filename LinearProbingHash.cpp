/**
 * @file LinearProbingHash.hpp
 * @author Jacob H. Schandel (jhs51@uakron.edu)
 * @brief Linear Probing Hash for Project 3: Hash
 * @version 0.1
 * @date 2021-04-23
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "LinearProbingHash.hpp"

/**
 * @brief Construct a new Linear Probing Hash Table object with 10 elements
 */
LinearProbingHash::LinearProbingHash()
{
    size = 10;
    hashTable = new int[10];
    inUse = new bool[10];
}

/**
 * @brief Construct a new Linear Probing Hash Table object
 * 
 * @param size number of slots in table
 */
LinearProbingHash::LinearProbingHash(int setSize)
{
    size = setSize;
    hashTable = new int[size];
    inUse = new bool[size];
}

/**
 * @brief Inserts an element into the hash table
 * 
 * @param value value to insert
 */
void LinearProbingHash::insert(int value)
{
    // calculate the hash value
    int hashValue = hash(value);

    // run linear probe calculation
    int probedValue = probe(value, hashValue);

    // insert value
    hashTable[probedValue] = value;
    inUse[probedValue] = true;
}

/**
 * @brief Removes an element from the hash table
 * 
 * @param value value to remove
 * @return int of removed value
 */
int LinearProbingHash::remove(int value)
{
    // calculate the hash value
    int hashValue = hash(value);

    // run linear probe calculation
    int probedValue = probe(value, hashValue);

    // remove value
    if (value == hashTable[probedValue])
    {
        int toReturn = hashTable[probedValue];
        inUse[probedValue] = false;
        return toReturn;
    }
    return 0;
}

/**
 * @brief Find a hash for a given value
 * 
 * @param value number to find
 * @return int containing hash value
 */
int LinearProbingHash::hash(int value)
{
    return value % size;
}

/**
 * @brief Function to probe the hash table for a given value and its hash
 * 
 * @param value number to find
 * @param hashValue hash value
 * @return int containing final probed value
 */
int LinearProbingHash::probe(int value, int hashValue)
{
    int current = hashValue;

    // search hash table
    while (hashTable[current] != 0 || hashTable[current] != value)
    {
        // increment current
        ++current;

        if (current == size)
            current = 0;

        // looped around the table; AKA "it's full!"
        if (current == hashValue)
            break;
    }

    return current;
}
