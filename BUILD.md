# Building Directions

To build this program, first create a build directory and move into it...

```bash
mkdir build
cd build
```

Once in your build directory, run `cmake`.  Assuming you are using a subdirectory of the project directory, run this...

```bash
cmake ..
```

***Note:** testing in this program used the generator Ninja.*

Use your generator tool with `run` to run this program.

```bash
# with make
make run

# with Ninja
ninja run
```
This is equivalent to the following:

```shell
./ProjectHash
```

Use `log` to output console to a text file.

```bash
# with make
make log

# with Ninja
ninja log
```

This is equivalent to the following:

```shell
./ProjectHash > output.txt
```