#include "2-ChoiceHashing.hpp"

TwoChoice::TwoChoice(int b)
{
    this->BUCKET = b;
    table = new list<int>[BUCKET];
}

void TwoChoice::insertItem(int key)
{
    int index1 = hashFunction1(key);
    int index2 = hashFunction2(key);
    //Get the amount of collisions in each hash index's bucket
    int collisions1 = table[index1].size();
    int collisions2 = table[index2].size();
   // cout << collisions1 << endl << collisions2;


    if(collisions1 <= collisions2)
    {
        table[index1].push_back(key);
    }
    else
    {
        table[index2].push_back(key);
    }

}

void TwoChoice::deleteItem(int key)
{
    //get the hash index of key
    int index1 = hashFunction1(key);
    int index2 = hashFunction2(key);

    //Find the key in (index)th list
    list <int> :: iterator i1;
    for (i1 = table[index1].begin(); i1 != table[index1].end(); i1++)
    {
        if(*i1 == key)
            break;
    }

    // if key is found in hash table, remove it
    if (i1 != table[index1].end())
        table[index1].erase(i1);
    else
    {
        list <int> :: iterator i2;
    for (i2 = table[index2].begin(); i2 != table[index2].end(); i2++)
    {
        if(*i2 == key)
            break;
    }

    // if key is found in hash table, remove it
    if (i2 != table[index2].end())
        table[index2].erase(i2);
    else
    {
        cout << "Not Found" << endl;
    }
    }
}

void TwoChoice::displayHash()
{
for (int i = 0; i < BUCKET; i++)
 {
    cout << i;
    for (auto x : table[i])
        cout << " --> " << x;
    cout << endl;
 }
}
 bool TwoChoice::Is_Prime(int Num) // function that checks if the number is prime
{
    int i = 2; // for iteration
    bool is_prime = 1; // the variable that says if number is prime or not
    float half = Num/2; // just so Number/2 wouldn't be calculated every iteration

    while( is_prime && i<=half ) // the while only makes max half iterations
    {
        if ( Num % i==0 )
          is_prime = 0;
        i++;
    }

    return is_prime; // returns 1 if it is prime and 0 if it isn't prime
}

int TwoChoice::LargestPrime(int Num) // function that finds the maximum prime number smaller than Number
{
	int MaxPrime = 0; // variable that stores the max prime number that the function will return
	bool found_maxp = 0; // variable that keeps track if the max prime number has been found
	int i = Num;

	while ( !found_maxp && i>2 ) // the while only makes max number-2 iterations
	{
	    if ( Is_Prime(i) )
	    {
          found_maxp = 1; // the max prime number has been found, no need to search next iteration
	      MaxPrime = i; // the max prime number is of course i
	    }
        i--;
	}

	return MaxPrime; // returns the max prime smaller or equal to Number
}
