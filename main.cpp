/**
 * @file main.cpp
 * @author Jacob H. Schandel (jhs51@uakron.edu)
 * @brief Main file for Project Hash
 * @version 1.01
 * @date 2021-03-09
 * 
 * @copyright Copyright (c) 2021
 * 
 */

/*
   ACADEMIC INTEGRITY PLEDGE

   - I have not used source code obtained from another
     xstudent nor any other unauthorized source, either
     modified or unmodified. Any source code used for
     testing that is another student's has been
     appropriately documented

   - All source code and documentation used in my program
     is either my original work or was derived by me from
     the source code published in the textbook for this
     course or presented in class. Other sources are acknowledged
     without transcribing code verbatim

   - I have not discussed coding details about this project
     with anyone other than my instructor, TA, and my immediate
     project team. I understand that I may discuss the concepts
     of this program with other students and that another student
     may help me debug my program so long as neither of us writes
     anything during the discussion or modifies any computer file
     during the discussion.

   - I have violated neither the spirit nor letter of these
     restrictions.

   Signed: Jacob H. Schandel Date: 03/09/2021


   COPYRIGHT (C) 2021 Jacob H. Schandel. All Rights Reserved.
   Project 3: Hash
   Jacob H. Schandel, jhs51
   version 1.01 2021-04-25
   Files: main.cpp
          Complexity_Timer.hpp
          Complexity_Recorder.hpp
          LinearProbingHash.hpp
          LinearProbingHash.cpp
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>

#include "Complexity_Recorder.hpp" // provided by Prof. Will
#include "Complexity_Timer.hpp"    // provided by Prof. Will
#include "LinearProbingHash.hpp"   // my hash table
#include <set>                     // STL basis of comparison
#include <unordered_set>           // STL basis of comparison

// number of structures to test
const int STRUCTURES = 3;
const int NUM_TRIALS = 20;
const int NUM_ELEMENTS = 50000;

// headers
const char *headings[STRUCTURES] = {"| LinProbingHash ", "|      set       ", "| unordered_set  "};

int main()
{
    // initializations
    LinearProbingHash * myHashTable;
    std::set<int> * stlSet;
    std::unordered_set<int> * stlUnorderedSet;

    // vectors to make executing tests after insert() easier
    std::vector<LinearProbingHash *> myHashTableStash(NUM_TRIALS);
    std::vector<std::set<int> *> stlSetStash(NUM_TRIALS);
    std::vector<std::unordered_set<int> *> stlUnorderedSetStash(NUM_TRIALS);

    // for the test
    srand(time(NULL));

    std::ofstream ofs("../raw_results.txt");

    // this is going to hold the measurements
    timer timer1;
    std::vector<recorder<timer>> stats(STRUCTURES);

    /***test of insert***/

    std::cout << "~~~~~~~~" << std::endl;
    std::cout << "insert()" << std::endl;
    std::cout << "~~~~~~~~" << std::endl;

    std::cout << "____";
    for (int i = 0; i < STRUCTURES; ++i)
        std::cout << headings[i];
    std::cout << "|________________" << std::endl;

    std::cout << "Tst#";
    for (int i = 0; i < STRUCTURES; ++i)
        std::cout << "|      Time      ";
    std::cout << "|      Reps      ";
    std::cout << std::endl;

    for (int j = 0; j < STRUCTURES; ++j)
        stats[j].reset();

    int repetitions;
    for (int i = 0; i < NUM_TRIALS; ++i)
    {
        myHashTable = new LinearProbingHash(NUM_ELEMENTS);
        stlSet = new std::set<int>();
        stlUnorderedSet = new std::unordered_set<int>();

        repetitions = ((i+1)/(double)NUM_TRIALS) * NUM_ELEMENTS / 2;

        std::cout << std::setw(4) << (i+1) << std::flush;
        ofs << std::setw(4) << (i+1);

        // test of my heap
        timer1.restart();
        for (int i = 0; i < repetitions; ++i)
            myHashTable->insert(rand());
        timer1.stop();
        stats[0].record(timer1);

        // reporting of my table
        stats[0].report(std::cout, 1);
        stats[0].report(ofs, 1);

        // test of set
        timer1.restart();
        for (int i = 0; i < repetitions; ++i)
            stlSet->insert(rand());
        timer1.stop();
        stats[1].record(timer1);

        // reporting of set
        stats[1].report(std::cout, 1);
        stats[1].report(ofs, 1);

        // test of unordered set
        timer1.restart();
        for (int i = 0; i < repetitions; ++i)
            stlUnorderedSet->insert(rand());
        timer1.stop();
        stats[2].record(timer1);

        // reporting of unordered set
        stats[2].report(std::cout, 1);
        stats[2].report(ofs, 1);
        
        // end of test
        std::cout << std::setw(12) << repetitions << std::endl;
        ofs << std::endl;

        myHashTableStash[i] = myHashTable;
        stlSetStash[i] = stlSet;
        stlUnorderedSetStash[i] = stlUnorderedSet;
    }

    std::cout << std::endl
              << std::endl;

    /***test of remove***/

    std::cout << "~~~~~~~~" << std::endl;
    std::cout << "remove()" << std::endl;
    std::cout << "~~~~~~~~" << std::endl;

    std::cout << "____";
    for (int i = 0; i < STRUCTURES; ++i)
        std::cout << headings[i];
    std::cout << "|________________" << std::endl;

    std::cout << "Tst#";
    for (int i = 0; i < STRUCTURES; ++i)
        std::cout << "|      Time      ";
    std::cout << "|      Reps      ";
    std::cout << std::endl;

    for (int j = 0; j < STRUCTURES; ++j)
        stats[j].reset();

    for (int i = 0; i < NUM_TRIALS; ++i)
    {
        myHashTable = myHashTableStash[i];
        stlSet = stlSetStash[i];
        stlUnorderedSet = stlUnorderedSetStash[i];

        repetitions = ((i+1)/(double)NUM_TRIALS) * NUM_ELEMENTS / 2;

        std::cout << std::setw(4) << (i+1) << std::flush;
        ofs << std::setw(4) << (i+1);

        // test of my heap
        timer1.restart();
        for (int i = 0; i < repetitions; ++i)
            myHashTable->remove(rand());
        timer1.stop();
        stats[0].record(timer1);

        // reporting of my table
        stats[0].report(std::cout, 1);
        stats[0].report(ofs, 1);

        // test of Benjamin's table
        timer1.restart();
        for (int i = 0; i < repetitions; ++i)
        {
            auto toRemove = stlSet->find(rand());
            if (toRemove != stlSet->end())
                stlSet->erase(toRemove);
        }
        timer1.stop();
        stats[1].record(timer1);

        // reporting of Benjamin's table
        stats[1].report(std::cout, 1);
        stats[1].report(ofs, 1);

        // test of unordered set
        timer1.restart();
        for (int i = 0; i < repetitions; ++i)
        {
            auto toRemove = stlUnorderedSet->find(rand());
            if (toRemove != stlUnorderedSet->end())
                stlUnorderedSet->erase(toRemove);
        }
        timer1.stop();
        stats[2].record(timer1);

        // reporting of unordered set
        stats[2].report(std::cout, 1);
        stats[2].report(ofs, 1);
        
        // end of test
        std::cout << std::setw(12) << repetitions << std::endl;
        ofs << std::endl;

        delete myHashTable;
        delete stlSet;
        delete stlUnorderedSet;
    }

    std::cout << std::endl
              << std::endl;

    return 0;
}
