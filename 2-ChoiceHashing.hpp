#include <iostream>
#include <cstdlib>
#include <string>
#include <cstdio>
#include <bits/stdc++.h>

using std::cout;
using std::endl;
using std::list;

class TwoChoice
{
   int BUCKET;  //Number of buckets

   list<int> *table; //Pointer to an array containing buckets

public:
    TwoChoice(int V); //Constructor

    bool Is_Prime(int Num);
    int LargestPrime(int Num); //Finds the largest prime below the bucket size for the
    //second hash function

    // inserts a key into hash table
    void insertItem(int x);

    //deletes a key from hash table
    void deleteItem(int key);

    //hash function 1 to map values to key
    int hashFunction1(int x)
    {
        return (x % BUCKET);
    }

    //hash function 2 to map values to key
    int hashFunction2(int x)
    {
        int PRIME = LargestPrime(x);
        return (PRIME - (x % PRIME));
    }

    void displayHash();
};
